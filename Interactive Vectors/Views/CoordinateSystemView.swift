//
//  CoordinateSystemView.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit


class CoordinateSystemView: UIView {
    
    // MARK: Variables
    
    var quadrant: UIView!
    
    
    // MARK: Initialisers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setGrid()
        self.setUpQuadrant()
        self.setCoordinateSystem()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: Private Methods
    
    private func setUpQuadrant() {
        let quadrantView = UIView(frame: CGRect(x: self.coordinatesCenter.x, y: self.frame.origin.y, width: self.frame.width / 2, height: self.frame.width))
        
        quadrantView.flipCoordinates()
        
        self.quadrant = quadrantView
        
        self.setCoordinatesLabels()
        
        self.addSubview(self.quadrant)
    }
    
    private func setCoordinatesLabels() {
        let quadrantPointsCount = ptsCount / 2
        
        let numberFormatter = NumberFormatter()
        
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
        
        for i in 1..<Int(quadrantPointsCount / self.gridSquareWidth) {
            let offset = (CGFloat(i) * self.gridSquareWidth)
            
            self.setPositiveHorizontalLabel(offsetLength: offset, numberFormatter: numberFormatter)
            self.setNegativeHorizontalLabel(offsetLength: offset, numberFormatter: numberFormatter)
            self.setPositiveVerticalLabel(offsetHeight: offset, numberFormatter: numberFormatter)
            self.setNegativeVerticalLabel(offsetHeight: offset, numberFormatter: numberFormatter)
        }
    }
    
    private func setPositiveHorizontalLabel(offsetLength: CGFloat, numberFormatter: NumberFormatter) {
        let labelOriginOffset: CGFloat = self.gridSquareWidth * 0.3
        
        let label = UILabel(frame: CGRect(x: self.quadrant.bounds.origin.x + offsetLength - labelOriginOffset, y: self.quadrant.bounds.origin.y - gridSquareWidth * 0.3, width: self.gridSquareWidth, height: self.gridSquareWidth))
        
        label.flipCoordinates()
        
        let labelOriginInPts = (label.frame.origin.x + labelOriginOffset) / pt
        let number           = NSNumber(value: Int(labelOriginInPts))
        
        label.font      = label.font.withSize(10)
        label.text      = numberFormatter.string(from: number)
        label.textColor = .black
        
        self.quadrant.addSubview(label)
    }
    
    private func setNegativeHorizontalLabel(offsetLength: CGFloat, numberFormatter: NumberFormatter) {
        let labelOriginOffset: CGFloat = self.gridSquareWidth * 0.3
        
        let label = UILabel(frame: CGRect(x: self.quadrant.bounds.origin.x - offsetLength - labelOriginOffset, y: self.quadrant.bounds.origin.y - gridSquareWidth * 0.3, width: self.gridSquareWidth, height: self.gridSquareWidth))
        
        label.flipCoordinates()
        
        let labelOriginInPts = (label.frame.origin.x + labelOriginOffset) / pt
        let number = NSNumber(value: Int(labelOriginInPts))
        
        label.font = label.font.withSize(10)
        label.text = numberFormatter.string(from: number)
        label.textColor = .black
        
        self.quadrant.addSubview(label)
    }
    
    private func setPositiveVerticalLabel(offsetHeight: CGFloat, numberFormatter: NumberFormatter) {
        let labelOriginOffset: CGFloat = self.gridSquareWidth * 0.5
        
        let label = UILabel(frame: CGRect(x: self.quadrant.bounds.origin.x + 1, y: self.quadrant.bounds.origin.y + offsetHeight - labelOriginOffset, width: self.gridSquareWidth, height: self.gridSquareWidth))
        
        label.flipCoordinates()
        
        let labelOriginInPts = (label.frame.origin.y + labelOriginOffset) / pt
        let number = NSNumber(value: Int(labelOriginInPts))
        
        label.font = label.font.withSize(10)
        label.text = numberFormatter.string(from: number)
        label.textColor = .black
        
        self.quadrant.addSubview(label)
    }
    
    private func setNegativeVerticalLabel(offsetHeight: CGFloat, numberFormatter: NumberFormatter) {
        let labelOriginOffset: CGFloat = self.gridSquareWidth * 0.5
        
        let label = UILabel(frame: CGRect(x: self.quadrant.bounds.origin.x + 1, y: self.quadrant.bounds.origin.y - offsetHeight - labelOriginOffset, width: self.gridSquareWidth, height: self.gridSquareWidth))
        
        label.flipCoordinates()
        
        let labelOriginInPts = (label.frame.origin.y + labelOriginOffset) / pt
        let number = NSNumber(value: Int(labelOriginInPts))
        
        label.font = label.font.withSize(10)
        label.text = numberFormatter.string(from: number)
        label.textColor = .black
        
        self.quadrant.addSubview(label)
    }
    
    private func setCoordinateSystem() {
        let yAxisView = UIView()
        
        yAxisView.frame.size      = CGSize(width: 1.5, height: self.frame.height)
        yAxisView.center          = self.coordinatesCenter
        yAxisView.backgroundColor = .black
        
        let xAxisView = UIView()
        
        xAxisView.frame.size      = CGSize(width: self.frame.width, height: 1.5)
        xAxisView.center.y        = self.frame.width
        xAxisView.backgroundColor = .black
        
        self.addSubview(yAxisView)
        self.addSubview(xAxisView)
    }
    
    private func setGrid() {
        let squaresCount = Int(ptsCount / self.gridSquareWidth)
        
        for i in 0..<squaresCount {
            let length = CGFloat(i) * self.gridSquareWidth
            
            let verticalGridView = UIView(frame: CGRect(x: self.frame.origin.x + length, y: self.frame.origin.y, width: 0.5, height: self.frame.height))
            
            let horizontalGridView = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y + length, width: self.frame.width, height: 0.5))
            
            horizontalGridView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
            verticalGridView.backgroundColor   = UIColor.lightGray.withAlphaComponent(0.5)
            
            self.addSubview(horizontalGridView)
            self.addSubview(verticalGridView)
        }
        
    }
    
    
    // MARK: Helpers
    
    lazy private var gridSquareWidth: CGFloat = {
        return 1250.0 * pt
    }()
    
    lazy private var coordinatesCenter: CGPoint = {
        return CGPoint(x: self.frame.width / 2, y: self.frame.width)
    }()
    
}
