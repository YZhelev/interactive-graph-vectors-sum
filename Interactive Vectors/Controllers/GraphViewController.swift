//
//  GraphController.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {
    
    // MARK: Constants
    
    private let kTabBarController: String = "tabBarController"
    
    
    // MARK: Variables
    
    private var storage: JSONStorage!
    
    private var coordinateSystemView: CoordinateSystemView!
    
    private var vector1Layer = CAShapeLayer()
    private var vector2Layer = CAShapeLayer()
    private var vector3Layer = CAShapeLayer()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Interactive graph"
        
        self.setCoordinateSystemView()
        
        self.storage = JSONStorage.shared
        
        self.drawInitialVectors()
        
        self.addTabBarController()
    }
    
    
    // MARK: Private Methods
    
    private func setCoordinateSystemView() {
        self.coordinateSystemView = CoordinateSystemView(frame: self.view.frame)
        self.view.addSubview(self.coordinateSystemView)
    }
    
    private func drawVector(angle: CGFloat, length: CGFloat, lineWidth: CGFloat?, strokeColor: UIColor, vectorLayer: inout CAShapeLayer) {
        let vAngle  = angle
        let vLength = length * 1000 * pt
        
        let vX = CGFloat(vLength * cos(vAngle * .pi / 180))
        let vY = CGFloat(vLength * sin(vAngle * .pi / 180))
        
        let path = UIBezierPath()
        
        path.addArrow(start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: vX, y: vY))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path        = path.cgPath
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth   = lineWidth ?? 1.0
        shapeLayer.fillColor   = UIColor.clear.cgColor
        shapeLayer.lineJoin    = .round
        shapeLayer.lineCap     = .round
        
        if self.coordinateSystemView.quadrant.layer.sublayers?.contains(vectorLayer) ?? false {
            vectorLayer.removeFromSuperlayer()
        }
        
        vectorLayer = shapeLayer
        
        self.coordinateSystemView.quadrant.layer.addSublayer(vectorLayer)
    }
    
    private func drawVector1() {
        guard
            let v1       = self.storage.vectors.first(where: {$0.id == "v1"}),
            let v1Angle  = v1.angleParam?.value,
            let v1Length = v1.lengthParam?.value else {
            return
        }
        
        self.drawVector(angle: CGFloat(v1Angle), length: CGFloat(v1Length), lineWidth: v1.style?.strokeWidth, strokeColor: v1.primaryColor, vectorLayer: &self.vector1Layer)
    }
    
    private func drawVector2() {
        guard
            let v2       = self.storage.vectors.first(where: {$0.id == "v2"}),
            let v2Angle  = v2.angleParam?.value,
            let v2Length = v2.lengthParam?.value else {
                return
        }
        
        self.drawVector(angle: CGFloat(v2Angle), length: CGFloat(v2Length), lineWidth: v2.style?.strokeWidth, strokeColor: v2.primaryColor, vectorLayer: &self.vector2Layer)
    }
    
    private func drawSumVector() {
        guard let vec3 = self.storage.vectors.first(where: {$0.id == "add"}), let v1path = self.vector1Layer.path, let v2path = self.vector2Layer.path else {
            return
        }
        
        let vX: CGFloat = v1path.currentPoint.x + v2path.currentPoint.x
        let vY: CGFloat = v1path.currentPoint.y + v2path.currentPoint.y
        
        let path = UIBezierPath()
        
        path.addArrow(start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: vX, y: vY))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path        = path.cgPath
        shapeLayer.strokeColor = vec3.primaryColor.cgColor
        shapeLayer.lineWidth   = vec3.style?.strokeWidth ?? 2.5
        shapeLayer.fillColor   = UIColor.clear.cgColor
        shapeLayer.lineJoin    = .round
        shapeLayer.lineCap     = .round
        
        if self.coordinateSystemView.quadrant.layer.sublayers?.contains(self.vector3Layer) ?? false {
            self.vector3Layer.removeFromSuperlayer()
        }
        
        self.vector3Layer = shapeLayer
        
        self.coordinateSystemView.quadrant.layer.addSublayer(self.vector3Layer)
    }
    
    private func drawInitialVectors() {
        self.drawVector1()
        self.drawVector2()
        self.drawSumVector()
    }
    
    private func addTabBarController() {
        self.addChild(self.childTabBarController)
        self.childTabBarController.view.frame = CGRect(x: 0.0, y: self.view.frame.height - self.childTabBarControllerHeightCollapsed, width: self.view.frame.width, height: self.childTabBarControllerHeight)
        self.view.addSubview(self.childTabBarController.view)
        self.childTabBarController.didMove(toParent: self)
        
        self.childTabBarController.viewControllers?.forEach { controller in
            if let tabController = controller as? TabViewController {
                tabController.delegate = self
            }
        }
    }
    
    
    // MARK: Helpers
    
    private lazy var childTabBarController: UITabBarController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        return storyBoard.instantiateViewController(withIdentifier: kTabBarController) as! TabBarController
    }()
    
    private lazy var childTabBarControllerHeight: CGFloat = {
        return self.view.frame.height / 2.5
    }()
    
    private lazy var childTabBarControllerHeightCollapsed: CGFloat = {
        return self.view.frame.height * 0.09
    }()
    
}

extension GraphViewController: TabViewControllerDelegate {
    
    // MARK: Tab Controller Delegate
    
    func headerViewTapped() {
        let isExpanded = self.childTabBarController.view.frame.origin.y < self.view.frame.height * 0.8
        
        UIView.animate(withDuration: 0.4, animations: {
            let yOrigin = isExpanded ? self.view.frame.height - self.childTabBarControllerHeightCollapsed
                                     : self.view.frame.height - self.childTabBarControllerHeight
            self.childTabBarController.view.frame.origin.y = yOrigin
        })
    }
    
    func hideShowVector(_ vector: Vector) {
        if vector.id == "v1" {
            vector1Layer.isHidden = !vector.show
        }
        
        if vector.id == "v2" {
            vector2Layer.isHidden = !vector.show
        }
        
        if vector.id == "add" {
            vector3Layer.isHidden = !vector.show
        }
    }
    
    func changeValueOfParameter(_ parameter: Parameter) {
        guard let vector = self.storage.vectors.first(where: { $0.angleParam?.id == parameter.id || $0.lengthParam?.id == parameter.id }) else {
            return
        }
        
        if parameter.isAngleParam {
            vector.angleParam?.setValue(parameter.value)
        }
        else {
            vector.lengthParam?.setValue(parameter.value)
        }
        
        if vector.id == "v1" {
            self.drawVector1()
        }
        
        if vector.id == "v2" {
            self.drawVector2()
        }
        
        self.drawSumVector()
    }
    
}
