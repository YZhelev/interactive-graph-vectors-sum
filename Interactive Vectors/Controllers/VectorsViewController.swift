//
//  VectorsViewController.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class VectorsViewController: TabViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var vectorsTableView: UITableView!
    
    // MARK: Constants
    
    private let kRowHeight: CGFloat = 60.0
    
    private let kVectorControllCellIdentifier = "vectorControlCell"
    
    
    // MARK: Variables
    
    var vectors: [Vector] = []
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.vectorsTableView.delegate   = self
        self.vectorsTableView.dataSource = self
        
        self.vectorsTableView.tableFooterView = UIView()
        
        self.vectors = JSONStorage.shared.vectors
    }

}

extension VectorsViewController: UITableViewDelegate {
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRowHeight
    }
    
}

extension VectorsViewController: UITableViewDataSource {
    
    // MARK: Table View Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vectors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kVectorControllCellIdentifier, for: indexPath)
        
        if let vectorCell = cell as? VectorControlTableViewCell {
            vectorCell.vector = self.vectors[indexPath.row]
            vectorCell.delegate = self.delegate
        }
        
        return cell
    }
    
}
