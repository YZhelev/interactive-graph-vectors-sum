//
//  ParametersViewController.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class ParametersViewController: TabViewController {

    // MARK: Outlets
    
    @IBOutlet weak var parametersTableView: UITableView!
    
    
    // MARK: Constants
    
    private let kRowHeight: CGFloat = 60.0
    
    private let kParameterControlCellIdentifier = "parameterControlCell"
    
    
    // MARK: Variables
    
    var parameters: [Parameter] = []
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parametersTableView.delegate   = self
        self.parametersTableView.dataSource = self
        
        self.parametersTableView.tableFooterView = UIView()
        
        let vectors = JSONStorage.shared.vectors
        let unsafeParams: [Parameter?]  = [
            vectors.first(where: {$0.id == "v1"})?.angleParam,
            vectors.first(where: {$0.id == "v1"})?.lengthParam,
            vectors.first(where: {$0.id == "v2"})?.angleParam,
            vectors.first(where: {$0.id == "v2"})?.lengthParam
        ]
        
        self.parameters = unsafeParams.compactMap{$0}
    }
    
}

extension ParametersViewController: UITableViewDelegate {
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRowHeight
    }
    
}

extension ParametersViewController: UITableViewDataSource {
    
    // MARK: Table View Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parameters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kParameterControlCellIdentifier, for: indexPath)
        
        if let parameterCell = cell as? ParameterControlTableViewCell {
            parameterCell.parameter = self.parameters[indexPath.row]
            parameterCell.delegate  = self.delegate
        }
        
        return cell
    }
    
}

