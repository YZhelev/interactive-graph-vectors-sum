//
//  TabViewController.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {

    // MARK: Outlets
    
    @IBOutlet weak var headerView: UIView!
    
    
    // MARK: Variables
    
    weak var delegate: TabViewControllerDelegate?
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addHeaderGesture()
    }
    
    
    // MARK: Private Methods
    
    private func addHeaderGesture() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.headerTapped))
        
        self.headerView.addGestureRecognizer(tapRecognizer)
    }
    
    @objc private func headerTapped() {
        self.delegate?.headerViewTapped()
    }
    
}

protocol TabViewControllerDelegate: class {
    
    func headerViewTapped()
    func hideShowVector(_ vector: Vector)
    func changeValueOfParameter(_ parameter: Parameter)
    
}
