//
//  TabBarController.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 20.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.shadowColor   = UIColor.black.cgColor
        self.view.layer.shadowOpacity = 0.3
        self.view.layer.shadowRadius  = 2.0
        
        let rect = CGRect(origin : self.view.frame.origin,
                          size   : CGSize(width: self.view.frame.width, height: 10.0))
        
        self.view.layer.shadowPath = UIBezierPath(rect: rect).cgPath
        
        UITabBarItem.appearance(whenContainedInInstancesOf: [TabBarController.self]).setTitleTextAttributes([
            .font : UIFont.systemFont(ofSize: 20.0) as Any], for: .normal)
    }

}
