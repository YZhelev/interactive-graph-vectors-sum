//
//  Constants.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 20.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit


let ptsCount: CGFloat = 10000.0

var pt: CGFloat = UIScreen.main.bounds.width / ptsCount

struct Keys {
    
    static let objects      = "objects"
    static let type         = "type"
    static let data         = "data"
    static let id           = "id"
    static let name         = "name"
    static let desc         = "desc"
    static let primaryColor = "primaryColor"
    static let show         = "show"
    static let dependsOn    = "dependsOn"
    static let style        = "style"
    static let strokeWidth  = "strokeWidth"
    static let value        = "value"
    static let min          = "min"
    static let max          = "max"
    static let hardMin      = "hardMin"
    static let hardMax      = "hardMax"
    static let steps        = "steps"
    static let adjustable   = "adjustable"
    
}

struct Tags {
    
    static let mtext = "mtext"
    static let mi    = "mi"
    static let mn    = "mn"
    
}
