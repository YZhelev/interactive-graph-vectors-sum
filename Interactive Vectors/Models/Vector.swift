//
//  Vector.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation
import UIKit

class Vector {
    
    // MARK: Private Properties
    
    private(set) var id           : String
    private(set) var name         : String
    private(set) var desc         : String
    private(set) var primaryColor : UIColor
    
    private(set) var show: Bool
    
    private(set) var style: Style?
    
    private(set) var dependsOn: [String]
    
    
    // MARK: Public Properties
    
    var angleParam  : Parameter?
    var lengthParam : Parameter?
    
    
    // MARK: Initialisers
    
    init(json: JSON) {
        self.id           = json.safeString(forKey: .id)
        self.name         = json.safeString(forKey: .name)
        self.desc         = json.safeString(forKey: .desc)
        self.primaryColor = UIColor.withHex(json.safeString(forKey: .primaryColor))
        
        self.show = json.safeBool(forKey: .show)
        
        self.dependsOn = json.safeArray(forKey: .dependsOn)
        
        if  let styleDict = json[.style] as? JSON,
            let strokeWidth = styleDict[.strokeWidth] as? CGFloat {
                self.style = Style(strokeWidth: strokeWidth)
        }
    }
    
    
    // MARK: Public Methods
    
    func showVector(_ show: Bool) {
        self.show = show
    }
    
}

struct Style {
    let strokeWidth: CGFloat
}
