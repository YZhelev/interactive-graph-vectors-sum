//
//  Parameter.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

class Parameter {
    
    // MARK: Enums
    
    enum `Type`: Int {
        case unknown
        case `default`
    }
    
    
    // MARK: Private Properties
    
    private(set) var id   : String
    private(set) var name : String
    private(set) var desc : String
    
    private(set) var value   : Double
    private(set) var min     : Double
    private(set) var max     : Double
    private(set) var hardMin : Double?
    private(set) var hardMax : Double?
    private(set) var steps   : Double
    
    private(set) var type: Type
    
    private(set) var adjustable: Bool

    
    // MARK: Initialisers
    
    init(json: JSON) {
        self.id    = json.safeString(forKey: .id)
        self.name  = json.safeString(forKey: .name).getMatchingValueFrom(tag: Tags.mtext)
        self.name += json.safeString(forKey: .name).getMatchingValueFrom(tag: Tags.mi)
        self.name += json.safeString(forKey: .name).getMatchingValueFrom(tag: Tags.mn)
        self.desc  = json.safeString(forKey: .desc)
        
        self.value   = json.safeDouble(forKey: .value)
        self.min     = json.safeDouble(forKey: .min)
        self.max     = json.safeDouble(forKey: .max)
        self.hardMin = json[.hardMin] as? Double
        self.hardMax = json[.hardMax] as? Double
        self.steps   = json.safeDouble(forKey: .steps)
        
        self.type = Type(rawValue: json.safeInt(forKey: .type)) ?? .unknown
        
        self.adjustable = json.safeBool(forKey: .adjustable)
    }
    
    
    // MARK: Public Methods
    
    var isAngleParam: Bool {
        return self.id.contains("angle")
    }
    
    func setValue(_ value: Double) {
        self.value = value
    }
    
}
