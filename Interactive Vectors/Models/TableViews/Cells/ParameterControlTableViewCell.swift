//
//  ParameterControlTableViewCell.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class ParameterControlTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var parameterTitleLabel : UILabel!
    @IBOutlet weak var parameterValueLabel : UILabel!
    
    @IBOutlet weak var valueSlider: UISlider!
    
    
    // MARK: Variables
    
    weak var delegate: TabViewControllerDelegate?
    
    var parameter: Parameter! {
        didSet {
           self.setupParameterCell()
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func didSlide(_ sender: UISlider) {
        self.parameter.setValue(Double(sender.value) * self.parameter.steps)
        
        self.parameterValueLabel.text = self.valueSlider.value.formattedWithSeparator
        
        self.delegate?.changeValueOfParameter(self.parameter)
    }
    
    
    // MARK: Private Methods
    
    private func setupParameterCell() {
        let title = self.parameter.isAngleParam ? parameter.name : String(parameter.name.suffix(2))
        
        if !title.isEmpty {
            self.parameterTitleLabel.attributedText = NSMutableAttributedString().vectorIndexSubscript(string: title, characters: [title.last!])
        }
        
        
        self.valueSlider.minimumValue = Float(self.parameter.min)
        self.valueSlider.maximumValue = Float(self.parameter.max) / Float(self.parameter.steps)
        self.valueSlider.isEnabled    = self.parameter.adjustable
        self.valueSlider.value        = Float(self.parameter.value) / Float(self.parameter.steps)
        
        self.parameterValueLabel.text = self.valueSlider.value.formattedWithSeparator
    }
}
