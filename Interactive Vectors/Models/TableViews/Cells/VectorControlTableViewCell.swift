//
//  VectorControlTableViewCell.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class VectorControlTableViewCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var vectorColorView: UIView!
    
    @IBOutlet weak var vectorTitleLabel: UILabel!
    
    @IBOutlet weak var showVectorSwitch: UISwitch!
    
    
    // MARK: Variables
    
    weak var delegate: TabViewControllerDelegate?
    
    var vector: Vector! {
        didSet {
            self.setupVectorCell()
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func showHideVector(_ sender: Any) {
        self.vector.showVector(self.showVectorSwitch.isOn)
        
        self.delegate?.hideShowVector(self.vector)
    }
    
    
    // MARK: Private Methods
    
    private func setupVectorCell() {
        self.vectorColorView.backgroundColor = self.vector.primaryColor
        self.vectorTitleLabel.text           = self.vector.name
        self.showVectorSwitch.isOn           = self.vector.show
    }
    
}

