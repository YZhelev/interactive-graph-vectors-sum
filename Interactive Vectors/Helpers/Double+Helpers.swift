//
//  Double+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 15.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension Double {
    
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }

    
}
