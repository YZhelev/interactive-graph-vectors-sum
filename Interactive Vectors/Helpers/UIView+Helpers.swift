//
//  UIView+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

extension UIView {
    
    func flipCoordinates() {
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 1, y: -1)
        transform.translatedBy(x: 0.0, y: -self.bounds.size.height)
        
        self.transform = transform
    }
    
}
