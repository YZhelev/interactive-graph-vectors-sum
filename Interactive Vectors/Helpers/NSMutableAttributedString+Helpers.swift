//
//  NSMutableAttributedString+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

import UIKit

extension NSMutableAttributedString {
    
    // MARK: Enums
    
    enum Scripting : Int {
        case aSub = -1
        case aSuper = 1
    }
    
    
    // MARK: Public Methods
    
    func vectorIndexSubscript(string: String, characters: [Character]) -> NSMutableAttributedString {
        return self.characterSubscriptAndSuperscript(string         : string,
                                                     characters     : characters,
                                                     type           : .aSub,
                                                     fontSize       : 16.0,
                                                     scriptFontSize : 12.0,
                                                     offSet         : 6,
                                                     length         : [1],
                                                     alignment      : .left)
    }
    
    
    // MARK: Private Methods
    
    private func characterSubscriptAndSuperscript(string         : String,
                                                  characters     : [Character],
                                                  type           : Scripting,
                                                  fontSize       : CGFloat,
                                                  scriptFontSize : CGFloat,
                                                  offSet         : Int,
                                                  length         : [Int],
                                                  alignment      : NSTextAlignment ) -> NSMutableAttributedString {
        let paraghraphStyle = NSMutableParagraphStyle()
        
        paraghraphStyle.alignment = alignment
        
        var scriptedCharaterLocation = Int()
        
        let stringFont = UIFont.systemFont(ofSize: fontSize)
        let scriptFont = UIFont.systemFont(ofSize: scriptFontSize)
        
        let attString = NSMutableAttributedString(string     : string,
                                                  attributes : [.font            : stringFont,
                                                                .foregroundColor : UIColor.black,
                                                                .paragraphStyle  : paraghraphStyle])
        
        let baseLineOffset = offSet * type.rawValue
        
        for (i,c) in string.enumerated() {
            for (theLength,aCharacter) in characters.enumerated() {
                if c == aCharacter {
                    scriptedCharaterLocation = i
                 
                    attString.setAttributes([.font            : scriptFont,
                                             .baselineOffset  : baseLineOffset,
                                             .foregroundColor : UIColor.black],
                                            range: NSRange(location : scriptedCharaterLocation,
                                                            length  : length[theLength]))
                    
                }
            }
        }
        
        return attString
    }
}
