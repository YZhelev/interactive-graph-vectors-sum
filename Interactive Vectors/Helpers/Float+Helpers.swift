//
//  Float+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension Float {
    
    var formattedString: String {
        return String(format: "%.0f", self)
    }
    
    var formattedWithSeparator: String {
        return NumberFormatter.withSeparator.string(for: self) ?? ""
    }
}
