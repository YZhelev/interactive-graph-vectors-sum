//
//  String+Keys.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension String {
    
    static let objects      = Keys.objects
    static let type         = Keys.type
    static let data         = Keys.data
    static let id           = Keys.id
    static let name         = Keys.name
    static let desc         = Keys.desc
    static let primaryColor = Keys.primaryColor
    static let show         = Keys.show
    static let dependsOn    = Keys.dependsOn
    static let style        = Keys.style
    static let strokeWidth  = Keys.strokeWidth
    static let value        = Keys.value
    static let min          = Keys.min
    static let max          = Keys.max
    static let hardMin      = Keys.hardMin
    static let hardMax      = Keys.hardMax
    static let steps        = Keys.steps
    static let adjustable   = Keys.adjustable
    
}
