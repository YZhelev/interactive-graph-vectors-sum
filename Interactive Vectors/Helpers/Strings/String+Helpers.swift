//
//  String+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 21.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension String {
    
    func getMatchingValueFrom(tag: String) -> String {
        let pattern : String = "<"+tag+">(.*?)</"+tag+">"
        let regexOptions = NSRegularExpression.Options.caseInsensitive
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: regexOptions)
            
            guard let textCheckingResult : NSTextCheckingResult = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: UInt(0)), range: NSMakeRange(0, self.count)) else {
                return ""
            }
            
            let matchRange : NSRange = textCheckingResult.range(at: 1)
            let match      : String  = (self as NSString).substring(with: matchRange)
            
            return match
        } catch {
            print(pattern + "<-- not found in string -->" + self )
            return ""
        }
    }
    
}
