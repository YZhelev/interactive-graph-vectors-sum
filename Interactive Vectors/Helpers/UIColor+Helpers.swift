//
//  UIColor+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 20.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func withHex(_ hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        let r, g, b: CGFloat
        
        let scanner = Scanner(string: cString)
        
        var hexNumber: UInt32 = 0
        scanner.scanHexInt32(&hexNumber)
        
        r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
        g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
        b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
}
