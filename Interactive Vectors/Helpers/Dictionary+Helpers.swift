//
//  Dictionary+Helpers.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension JSON {
    
    func safeString(forKey key: String) -> String {
        return self[key] as? String ?? ""
    }
    
    func safeBool(forKey key: String) -> Bool {
        return self[key] as? Bool ?? false
    }
    
    func safeInt(forKey key: String) -> Int {
        return self[key] as? Int ?? 0
    }
    
    func safeDouble(forKey key: String) -> Double {
        return self[key] as? Double ?? 0.0
    }
    
    func safeArray<T>(forKey key: String) -> Array<T> {
        return self[key] as? Array ?? []
    }
    
}
