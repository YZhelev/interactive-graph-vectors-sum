//
//  FileStorage.swift
//  Interactive Vectors
//
//  Created by MacBook Pro on 14.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation


typealias JSON = [String : Any]

class JSONStorage {
    
    // MARK: Constants
    
    private let kFileName: String = "vectorsData"
    private let kFileType: String = "json"
    
    
    // MARK: Variables
    
    var vectors: [Vector] = []
    
    private var parameters: [Parameter] = []
    
    
    // MARK: Singleton
    
    static let shared = JSONStorage()
    
    
    // MARK: Initializers
    
    private init() {
        self.fetchJSON()
    }
    
    
    // MARK: Private Methods
    
    private func fetchJSON() {
        if let path = Bundle.main.path(forResource: kFileName, ofType: kFileType) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                if let jsonResult = jsonResult as? JSON, let objects = jsonResult[.objects] as? [JSON] {
                    objects.forEach { obj in
                        guard
                            let type    = obj[.type] as? String,
                            let objData = obj[.data] as? JSON else {
                                assertionFailure("Wrong JSON creation")
                                return
                        }
                        
                        if type == "vector" {
                            self.vectors.append(Vector(json: objData))
                        }
                        
                        if type == "parameter" {
                            self.parameters.append(Parameter(json: objData))
                        }
                    }
                    
                    self.vectors.forEach { (vect) in
                        let params = self.parameters.filter {vect.dependsOn.contains($0.id)}
                        
                        vect.angleParam  = params.first(where: {$0.isAngleParam})
                        vect.lengthParam = params.first(where: {!$0.isAngleParam})
                    }
                }
            } catch {
                print("failed to load data: \(error)")
            }
        }
    }
}
